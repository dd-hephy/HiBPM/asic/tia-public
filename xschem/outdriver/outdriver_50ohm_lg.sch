v {xschem version=3.1.0 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
T {Bias
240µA} 2410 -650 0 0 0.3 0.3 {}
T {Noise filter} 2560 -500 0 0 0.2 0.2 {}
T {~ 50 Ohm resistors} 4070 -830 0 0 0.4 0.4 {}
T {~ 8 mA} 3740 -180 0 0 0.4 0.4 {}
T {~ 1 mA} 3110 -170 0 0 0.4 0.4 {}
N 2350 -230 2430 -230 { lab=VN}
N 2470 -590 2490 -590 { lab=I_Bias}
N 2530 -390 2530 -300 { lab=VM18D}
N 2490 -270 2490 -260 { lab=VN}
N 2490 -360 2490 -330 { lab=#net1}
N 2400 -390 2490 -390 { lab=VN}
N 2400 -390 2400 -260 { lab=VN}
N 2400 -300 2490 -300 { lab=VN}
N 2490 -420 2530 -420 { lab=VM18D}
N 2530 -420 2530 -390 { lab=VM18D}
N 2490 -260 2490 -230 { lab=VN}
N 2400 -260 2400 -230 { lab=VN}
N 2660 -250 2660 -230 { lab=VN}
N 2660 -380 2660 -370 { lab=#net2}
N 2660 -440 2680 -440 { lab=VN}
N 2340 -890 2840 -890 { lab=VP}
N 2430 -230 4260 -230 { lab=VN}
N 2660 -310 2660 -250 { lab=VN}
N 2490 -440 2490 -420 { lab=VM18D}
N 2400 -470 2490 -470 { lab=VM18D}
N 2530 -500 2530 -470 { lab=I_Bias}
N 2490 -500 2530 -500 { lab=I_Bias}
N 2490 -590 2490 -500 { lab=I_Bias}
N 2580 -470 2580 -460 { lab=I_Bias}
N 2530 -470 2580 -470 { lab=I_Bias}
N 2530 -420 2550 -420 { lab=VM18D}
N 2610 -420 2610 -210 { lab=#net2}
N 2610 -370 2660 -370 { lab=#net2}
N 2610 -210 4120 -210 { lab=#net2}
N 2680 -440 2760 -440 {
lab=VN}
N 2760 -440 2760 -230 {
lab=VN}
N 2760 -940 2760 -440 {
lab=VN}
N 3120 -250 3120 -230 { lab=VN}
N 2910 -550 2950 -550 { lab=InputSignal}
N 2990 -520 2990 -500 { lab=VM5D}
N 2990 -550 3090 -550 { lab=VN}
N 3250 -520 3250 -500 { lab=VM5D}
N 3250 -620 3250 -580 { lab=V_da2_N}
N 3120 -340 3220 -340 { lab=VN}
N 3120 -430 3220 -430 { lab=VN}
N 3120 -400 3120 -370 { lab=#net3}
N 3080 -430 3080 -340 { lab=#net2}
N 3220 -430 3220 -230 { lab=VN}
N 2990 -500 3250 -500 { lab=VM5D}
N 3120 -500 3120 -460 { lab=VM5D}
N 3290 -550 3320 -550 { lab=InputRef}
N 3080 -340 3080 -210 { lab=#net2}
N 3220 -550 3250 -550 {
lab=VN}
N 3090 -550 3220 -550 {
lab=VN}
N 2990 -890 2990 -850 {
lab=VP}
N 3250 -890 3250 -850 {
lab=VP}
N 3250 -850 3250 -840 {
lab=VP}
N 2990 -780 2990 -740 {
lab=V_da2_P}
N 2990 -850 2990 -840 {
lab=VP}
N 3010 -810 3230 -810 {
lab=VN}
N 2760 -940 3130 -940 {
lab=VN}
N 3130 -940 3130 -810 {
lab=VN}
N 2840 -890 4090 -890 {
lab=VP}
N 2400 -440 2490 -440 {
lab=VM18D}
N 2400 -470 2400 -440 {
lab=VM18D}
N 2550 -420 2580 -420 {
lab=VM18D}
N 2610 -760 2610 -730 {
lab=VN}
N 2610 -730 2760 -730 {
lab=VN}
N 2480 -730 2610 -730 {
lab=VN}
N 2480 -760 2480 -730 {
lab=VN}
N 2480 -890 2480 -820 {
lab=VP}
N 2610 -890 2610 -820 {
lab=VP}
N 3120 -310 3120 -250 {
lab=VN}
N 3800 -250 3800 -230 { lab=VN}
N 3590 -550 3630 -550 { lab=V_da2_P}
N 3670 -520 3670 -500 { lab=VM15D}
N 3670 -550 3770 -550 { lab=VN}
N 3930 -520 3930 -500 { lab=VM15D}
N 3930 -620 3930 -580 { lab=#net4}
N 3800 -340 3900 -340 { lab=VN}
N 3800 -430 3900 -430 { lab=VN}
N 3800 -400 3800 -370 { lab=#net5}
N 3760 -430 3760 -340 { lab=#net2}
N 3900 -430 3900 -230 { lab=VN}
N 3670 -500 3930 -500 { lab=VM15D}
N 3800 -500 3800 -460 { lab=VM15D}
N 3970 -550 4000 -550 { lab=V_da2_N}
N 3760 -340 3760 -210 { lab=#net2}
N 3900 -550 3930 -550 {
lab=VN}
N 3770 -550 3900 -550 {
lab=VN}
N 3670 -890 3670 -850 {
lab=VP}
N 3930 -890 3930 -850 {
lab=VP}
N 3930 -790 3930 -780 {
lab=#net4}
N 3930 -850 3930 -840 {
lab=VP}
N 3670 -780 3670 -730 {
lab=OutN}
N 3670 -850 3670 -840 {
lab=VP}
N 3690 -810 3910 -810 {
lab=VN}
N 3810 -940 3810 -810 {
lab=VN}
N 3800 -310 3800 -250 {
lab=VN}
N 3130 -940 3810 -940 {
lab=VN}
N 3550 -720 3550 -550 {
lab=V_da2_P}
N 3550 -550 3590 -550 {
lab=V_da2_P}
N 4000 -550 4040 -550 {
lab=V_da2_N}
N 4160 -660 4260 -660 {
lab=#net4}
N 4160 -730 4160 -660 {
lab=#net4}
N 3930 -730 4160 -730 {
lab=#net4}
N 3670 -750 4260 -750 {
lab=OutN}
N 2990 -740 2990 -710 {
lab=V_da2_P}
N 2990 -650 2990 -580 {
lab=V_da2_P}
N 3570 -740 3570 -640 {
lab=V_da2_N}
N 3570 -640 4040 -640 {
lab=V_da2_N}
N 4040 -640 4040 -550 {
lab=V_da2_N}
N 3250 -650 3250 -620 {
lab=V_da2_N}
N 3250 -780 3250 -710 {
lab=V_da2_N}
N 3930 -780 3930 -720 {
lab=#net4}
N 3670 -730 3670 -720 {
lab=OutN}
N 3670 -660 3670 -580 {
lab=OutN}
N 3930 -660 3930 -620 {
lab=#net4}
N 3220 -550 3220 -430 {
lab=VN}
N 2990 -740 3550 -740 {
lab=V_da2_P}
N 3250 -760 3570 -760 {
lab=V_da2_N}
N 2990 -710 2990 -650 {
lab=V_da2_P}
N 3250 -710 3250 -650 {
lab=V_da2_N}
N 3930 -720 3930 -660 {
lab=#net4}
N 3670 -720 3670 -660 {
lab=OutN}
N 3900 -550 3900 -430 {
lab=VN}
N 2700 -890 2700 -820 {
lab=VP}
N 2700 -760 2700 -730 {
lab=VN}
N 3300 -550 3300 -420 {
lab=InputRef}
N 3300 -360 3300 -230 {
lab=VN}
N 3550 -740 3550 -720 {
lab=V_da2_P}
N 3570 -760 3570 -740 {
lab=V_da2_N}
C {devices/iopin.sym} 2350 -230 0 1 {name=p1 lab=VN}
C {devices/ipin.sym} 3320 -550 0 1 {name=p2 lab=InputRef}
C {devices/iopin.sym} 2340 -890 0 1 {name=p4 lab=VP}
C {devices/ipin.sym} 2470 -590 0 0 {name=p5 lab=I_Bias}
C {devices/ipin.sym} 2910 -550 0 0 {name=p6 lab=InputSignal}
C {devices/ngspice_get_value.sym} 2620 -160 0 0 {name=r5 node="@m.x2.xm2.msky130_fd_pr__pfet_01v8_lvt[gm]"
descr="M2 gm"}
C {devices/ngspice_get_value.sym} 2670 -160 0 0 {name=r2 node="@m.x2.xm2.msky130_fd_pr__pfet_01v8_lvt[gds]"
descr="M2 gds"}
C {devices/ngspice_get_value.sym} 2570 -160 0 0 {name=r3 node="@m.x2.xm1.msky130_fd_pr__nfet_01v8_lvt[gm]"
descr="M1 gm"}
C {devices/ngspice_get_value.sym} 2520 -160 0 0 {name=r4 node="@m.x2.xm1.msky130_fd_pr__nfet_01v8_lvt[gds]"
descr="M1 gds"}
C {devices/ngspice_get_value.sym} 2780 -160 0 0 {name=r6 node="@m.x2.xm3.msky130_fd_pr__nfet_01v8[gm]"
descr="M3 gm"}
C {devices/ngspice_get_value.sym} 2730 -160 0 0 {name=r7 node="@m.x2.xm3.msky130_fd_pr__nfet_01v8[gds]"
descr="M3 gds"}
C {devices/opin.sym} 4260 -750 0 0 {name=p8 lab=OutN}
C {devices/opin.sym} 4260 -660 0 0 {name=p9 lab=OutP}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2510 -300 0 1 {name=M17
L=0.3
W=2
nf=1
mult=6
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2510 -390 0 1 {name=M18
L=0.15
W=2
nf=1
mult=12
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2510 -470 0 1 {name=M7
L=0.15
W=2
nf=1
mult=20
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {devices/lab_wire.sym} 2990 -740 0 0 {name=l2 sig_type=std_logic lab=V_da2_P}
C {devices/lab_wire.sym} 3270 -760 0 1 {name=l5 sig_type=std_logic lab=V_da2_N}
C {devices/ngspice_get_value.sym} 3140 -240 0 0 {name=r14 node="i(v.xoutd.v2)"
descr="i(v.xoutd.v2)"}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3100 -430 0 0 {name=M5
L=0.15
W=2
nf=1
mult=12*5
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3100 -340 0 0 {name=M8
L=0.3
W=2
nf=1
mult=6*5
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {devices/lab_wire.sym} 3120 -500 0 1 {name=l9 sig_type=std_logic lab=VM5D}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2580 -440 1 0 {name=M6
L=8
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {devices/lab_wire.sym} 2490 -420 0 0 {name=l1 sig_type=std_logic lab=VM18D}
C {sky130_fd_pr/cap_mim_m3_1.sym} 2480 -790 2 0 {name=C1 model=cap_mim_m3_1 W=15 L=30 MF=1 spiceprefix=X}
C {devices/ngspice_get_value.sym} 3820 -240 0 0 {name=r16 node="i(v.xoutd.v2)"
descr="i(v.xoutd.v2)"}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3780 -430 0 0 {name=M15
L=0.15
W=2
nf=1
mult=34*12
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3780 -340 0 0 {name=M16
L=0.3
W=2
nf=1
mult=34*6
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {devices/lab_wire.sym} 3800 -500 0 1 {name=l6 sig_type=std_logic lab=VM15D}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3950 -550 0 1 {name=M4
L=0.15
W=2
nf=1
mult=30
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2970 -550 0 0 {name=M23
L=0.15
W=2
nf=1
mult=6
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3650 -550 0 0 {name=M19
L=0.15
W=2
nf=1
mult=30
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3270 -550 0 1 {name=M14
L=0.15
W=2
nf=1
mult=6
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/res_high_po_1p41.sym} 3930 -810 0 0 {name=R12
W=1.41
L=4
model=res_high_po_1p41
spiceprefix=X
mult=22}
C {sky130_fd_pr/res_high_po_1p41.sym} 3670 -810 0 1 {name=R13
W=1.41
L=4
model=res_high_po_1p41
spiceprefix=X
mult=22}
C {sky130_fd_pr/res_xhigh_po_1p41.sym} 2990 -810 0 1 {name=R1
W=1.41
L=2
model=res_xhigh_po_1p41
spiceprefix=X
mult=2*4}
C {sky130_fd_pr/res_xhigh_po_1p41.sym} 3250 -810 0 0 {name=R15
W=1.41
L=2
model=res_xhigh_po_1p41
spiceprefix=X
mult=2*4}
C {sky130_fd_pr/cap_mim_m3_1.sym} 3300 -390 2 1 {name=C6 model=cap_mim_m3_1 W=10 L=6 MF=1 spiceprefix=X}
C {sky130_fd_pr/cap_mim_m3_1.sym} 2610 -790 2 0 {name=C2 model=cap_mim_m3_1 W=15 L=30 MF=1 spiceprefix=X}
C {sky130_fd_pr/cap_mim_m3_1.sym} 2700 -790 2 0 {name=C4 model=cap_mim_m3_1 W=15 L=30 MF=1 spiceprefix=X}
C {sky130_fd_pr/cap_mim_m3_1.sym} 2660 -410 0 0 {name=C3 model=cap_mim_m3_1 W=10 L=16 MF=1 spiceprefix=X}
