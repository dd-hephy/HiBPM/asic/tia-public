v {xschem version=3.1.0 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
T {Bias
60µA} 1500 -650 0 0 0.3 0.3 {}
T {4mA} 2440 -190 0 0 0.3 0.3 {}
T {Noise filter} 1650 -500 0 0 0.2 0.2 {}
T {800R
min. 3 µm width
==> L=6 N=3 W=1.41} 2420 -680 0 0 0.2 0.2 {}
T {5mA each} 3140 -760 0 0 0.2 0.2 {}
N 2480 -250 2480 -230 { lab=VN}
N 2270 -550 2310 -550 { lab=InputSignal}
N 1440 -230 2380 -230 { lab=VN}
N 2350 -520 2350 -500 { lab=VM3D}
N 2350 -550 2450 -550 { lab=VN}
N 2610 -520 2610 -500 { lab=VM3D}
N 2610 -620 2610 -580 { lab=V_da1_N}
N 1560 -590 1580 -590 { lab=I_Bias}
N 2480 -340 2580 -340 { lab=VN}
N 2480 -430 2580 -430 { lab=VN}
N 2480 -400 2480 -370 { lab=VM6D}
N 1620 -390 1620 -300 { lab=#net1}
N 2440 -430 2440 -340 { lab=#net2}
N 1580 -270 1580 -260 { lab=VN}
N 1580 -360 1580 -330 { lab=#net3}
N 1490 -390 1580 -390 { lab=VN}
N 1490 -390 1490 -260 { lab=VN}
N 1490 -300 1580 -300 { lab=VN}
N 2580 -430 2580 -230 { lab=VN}
N 1580 -420 1620 -420 { lab=#net1}
N 1620 -420 1620 -390 { lab=#net1}
N 1580 -260 1580 -230 { lab=VN}
N 1490 -260 1490 -230 { lab=VN}
N 1750 -250 1750 -230 { lab=VN}
N 1750 -380 1750 -370 { lab=#net2}
N 1750 -440 1770 -440 { lab=VN}
N 1430 -1180 2790 -1180 { lab=VP}
N 2350 -500 2610 -500 { lab=VM3D}
N 2480 -500 2480 -460 { lab=VM3D}
N 2790 -1180 3610 -1180 { lab=VP}
N 2380 -230 3580 -230 { lab=VN}
N 2650 -550 2680 -550 { lab=InputRef}
N 2440 -340 2440 -210 { lab=#net2}
N 1750 -310 1750 -250 { lab=VN}
N 1580 -440 1580 -420 { lab=#net1}
N 1490 -470 1580 -470 { lab=#net1}
N 1620 -500 1620 -470 { lab=I_Bias}
N 1580 -500 1620 -500 { lab=I_Bias}
N 1580 -590 1580 -500 { lab=I_Bias}
N 1670 -470 1670 -460 { lab=I_Bias}
N 1620 -470 1670 -470 { lab=I_Bias}
N 1620 -420 1640 -420 { lab=#net1}
N 1700 -420 1700 -210 { lab=#net2}
N 1700 -370 1750 -370 { lab=#net2}
N 1670 -420 1670 -230 { lab=VN}
N 1700 -210 3440 -210 { lab=#net2}
N 1770 -440 1850 -440 {
lab=VN}
N 1850 -440 1850 -230 {
lab=VN}
N 2580 -550 2610 -550 {
lab=VN}
N 2580 -550 2580 -430 {
lab=VN}
N 2450 -550 2580 -550 {
lab=VN}
N 2030 -250 2030 -230 { lab=VN}
N 2030 -340 2130 -340 { lab=VN}
N 2030 -430 2130 -430 { lab=VN}
N 2030 -400 2030 -370 { lab=#net4}
N 1990 -430 1990 -340 { lab=#net2}
N 2130 -430 2130 -230 { lab=VN}
N 1990 -340 1990 -210 { lab=#net2}
N 2030 -950 2030 -460 {
lab=#net5}
N 3150 -580 3150 -370 {
lab=OutputP}
N 3300 -580 3300 -370 {
lab=OutputN}
N 3150 -310 3150 -230 {
lab=VN}
N 3300 -310 3300 -230 {
lab=VN}
N 1990 -1020 1990 -950 {
lab=#net5}
N 1990 -950 2030 -950 {
lab=#net5}
N 2030 -1020 2120 -1020 {
lab=VP}
N 2120 -1180 2120 -1020 {
lab=VP}
N 1990 -1110 1990 -1020 {
lab=#net5}
N 2030 -990 2030 -950 {
lab=#net5}
N 2030 -1110 2120 -1110 {
lab=VP}
N 2030 -1080 2030 -1050 {
lab=#net6}
N 2030 -1180 2030 -1140 {
lab=VP}
N 2350 -1020 2440 -1020 {
lab=VP}
N 2440 -1180 2440 -1020 {
lab=VP}
N 2310 -1110 2310 -1020 {
lab=#net5}
N 2350 -1110 2440 -1110 {
lab=VP}
N 2350 -1080 2350 -1050 {
lab=#net7}
N 2350 -1180 2350 -1140 {
lab=VP}
N 2350 -990 2350 -580 {
lab=V_da1_P}
N 2030 -950 2310 -950 {
lab=#net5}
N 2310 -1020 2310 -950 {
lab=#net5}
N 2610 -1020 2700 -1020 {
lab=VP}
N 2700 -1180 2700 -1020 {
lab=VP}
N 2570 -1110 2570 -1020 {
lab=#net5}
N 2610 -1110 2700 -1110 {
lab=VP}
N 2610 -1080 2610 -1050 {
lab=#net8}
N 2610 -1180 2610 -1140 {
lab=VP}
N 2610 -990 2610 -620 {
lab=V_da1_N}
N 2570 -1020 2570 -950 {
lab=#net5}
N 2310 -950 2570 -950 {
lab=#net5}
N 3060 -1180 3060 -1020 {
lab=VP}
N 2930 -1110 2930 -1020 {
lab=#net5}
N 2970 -1110 3060 -1110 {
lab=VP}
N 2970 -1080 2970 -1050 {
lab=#net9}
N 2970 -1180 2970 -1140 {
lab=VP}
N 2930 -1020 2930 -950 {
lab=#net5}
N 2970 -990 2970 -640 {
lab=VM21S}
N 3300 -720 3300 -640 {
lab=V_da1_P}
N 2350 -720 3300 -720 {
lab=V_da1_P}
N 2570 -950 2930 -950 {
lab=#net5}
N 2610 -700 3150 -700 {
lab=V_da1_N}
N 3150 -700 3150 -640 {
lab=V_da1_N}
N 2970 -580 2970 -550 {
lab=VM21D}
N 2930 -550 2970 -550 {
lab=VM21D}
N 2930 -610 2930 -550 {
lab=VM21D}
N 2970 -610 3060 -610 {
lab=VP}
N 3060 -800 3060 -610 {
lab=VP}
N 3300 -610 3390 -610 {
lab=VP}
N 3390 -800 3390 -610 {
lab=VP}
N 3060 -800 3390 -800 {
lab=VP}
N 3150 -610 3220 -610 {
lab=VP}
N 3220 -800 3220 -610 {
lab=VP}
N 3060 -1020 3060 -800 {
lab=VP}
N 3110 -610 3110 -550 {
lab=VM21D}
N 2970 -550 3110 -550 {
lab=VM21D}
N 3260 -610 3260 -560 {
lab=VM21D}
N 3260 -560 3260 -550 {
lab=VM21D}
N 3110 -550 3260 -550 {
lab=VM21D}
N 3300 -510 3460 -510 {
lab=OutputN}
N 3150 -480 3460 -480 {
lab=OutputP}
N 2970 -250 2970 -230 { lab=VN}
N 2970 -340 3070 -340 { lab=VN}
N 2970 -430 3070 -430 { lab=VN}
N 2970 -400 2970 -370 { lab=#net10}
N 2930 -430 2930 -340 { lab=#net2}
N 3070 -430 3070 -230 { lab=VN}
N 2930 -340 2930 -210 { lab=#net2}
N 2970 -550 2970 -460 {
lab=VM21D}
N 1490 -440 1580 -440 {
lab=#net1}
N 1490 -470 1490 -440 {
lab=#net1}
N 2970 -1020 3060 -1020 {
lab=VP}
C {devices/ngspice_get_value.sym} 2350 -610 0 0 {name=r55 node="v(xoutd.v_da1_P)"
descr="v(xoutd.v_da1_P)"}
C {devices/iopin.sym} 1440 -230 0 1 {name=p1 lab=VN}
C {devices/ipin.sym} 2680 -550 0 1 {name=p2 lab=InputRef}
C {devices/iopin.sym} 1430 -1180 0 1 {name=p4 lab=VP}
C {devices/ipin.sym} 1560 -590 0 0 {name=p5 lab=I_Bias}
C {devices/ipin.sym} 2270 -550 0 0 {name=p6 lab=InputSignal}
C {devices/ngspice_get_value.sym} 2100 -160 0 0 {name=r5 node="@m.x2.xm2.msky130_fd_pr__pfet_01v8_lvt[gm]"
descr="M2 gm"}
C {devices/ngspice_get_value.sym} 2150 -160 0 0 {name=r2 node="@m.x2.xm2.msky130_fd_pr__pfet_01v8_lvt[gds]"
descr="M2 gds"}
C {devices/ngspice_get_value.sym} 2050 -160 0 0 {name=r3 node="@m.x2.xm1.msky130_fd_pr__nfet_01v8_lvt[gm]"
descr="M1 gm"}
C {devices/ngspice_get_value.sym} 2000 -160 0 0 {name=r4 node="@m.x2.xm1.msky130_fd_pr__nfet_01v8_lvt[gds]"
descr="M1 gds"}
C {devices/ngspice_get_value.sym} 2260 -160 0 0 {name=r6 node="@m.x2.xm3.msky130_fd_pr__nfet_01v8[gm]"
descr="M3 gm"}
C {devices/ngspice_get_value.sym} 2210 -160 0 0 {name=r7 node="@m.x2.xm3.msky130_fd_pr__nfet_01v8[gds]"
descr="M3 gds"}
C {devices/lab_wire.sym} 2350 -610 0 0 {name=l1 sig_type=std_logic lab=V_da1_P}
C {devices/ngspice_get_value.sym} 2610 -610 0 1 {name=r8 node="v(xoutd.v_da1_N)"
descr="v(xoutd.v_da1_N)"}
C {devices/ngspice_get_value.sym} 2420 -370 0 1 {name=r9 node="v(xoutd.vm3d)"
descr="v(xoutd.vm3d)"}
C {devices/ngspice_get_value.sym} 2430 -460 0 1 {name=r1 node="v(xoutd.vm6d)"
descr="v(xoutd.vm6d)"}
C {devices/lab_wire.sym} 2610 -610 0 1 {name=l8 sig_type=std_logic lab=V_da1_N}
C {devices/opin.sym} 3460 -510 0 0 {name=p8 lab=OutputN}
C {devices/opin.sym} 3460 -480 0 0 {name=p9 lab=OutputP}
C {devices/ngspice_get_value.sym} 2500 -240 0 0 {name=r16 node="i(v.xoutd.v1)"
descr="i(v.xoutd.v1)"}
C {sky130_fd_pr/cap_mim_m3_1.sym} 1750 -340 2 1 {name=C5 model=cap_mim_m3_1 W=20 L=20 MF=1 spiceprefix=X}
C {sky130_fd_pr/cap_mim_m3_1.sym} 1750 -410 0 0 {name=C3 model=cap_mim_m3_1 W=20 L=20 MF=1 spiceprefix=X}
C {devices/vsource.sym} 2480 -280 0 0 {name=V1 value=0}
C {sky130_fd_pr/nfet_01v8.sym} 1670 -440 3 1 {name=M16
L=6
W=2
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 1600 -300 0 1 {name=M17
L=0.5
W=2
nf=1
mult=10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 1600 -390 0 1 {name=M18
L=0.15
W=2
nf=1
mult=10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 1600 -470 0 1 {name=M7
L=0.15
W=2
nf=1
mult=30
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2330 -550 0 0 {name=M1
L=0.15
W=2
nf=1
mult=160
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2460 -430 0 0 {name=M3
L=0.15
W=2
nf=1
mult=40*10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {devices/res.sym} 3150 -340 0 0 {name=R3
value=50
footprint=1206
device=resistor
m=1}
C {sky130_fd_pr/pfet_01v8.sym} 3130 -610 0 0 {name=M2
L=0.15
W=2
nf=1
mult=140*20
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {devices/lab_wire.sym} 2480 -370 0 1 {name=l3 sig_type=std_logic lab=VM6D}
C {devices/lab_wire.sym} 2480 -460 0 1 {name=l4 sig_type=std_logic lab=VM3D}
C {devices/ngspice_get_value.sym} 2050 -240 0 0 {name=r10 node="i(v.xoutd.v2)"
descr="i(v.xoutd.v2)"}
C {devices/vsource.sym} 2030 -280 0 0 {name=V2 value=0}
C {sky130_fd_pr/pfet_01v8.sym} 2010 -1110 0 0 {name=M8
L=0.5
W=2
nf=1
mult=20
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 2010 -1020 0 0 {name=M9
L=0.15
W=2
nf=1
mult=20
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 3280 -610 0 0 {name=M11
L=0.15
W=2
nf=1
mult=140*20
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 2950 -610 0 0 {name=M21
L=0.15
W=2
nf=1
mult=20
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {devices/res.sym} 3300 -340 0 0 {name=R1
value=50
footprint=1206
device=resistor
m=1}
C {devices/lab_wire.sym} 2970 -510 0 1 {name=l5 sig_type=std_logic lab=VM21D}
C {devices/ngspice_get_value.sym} 2960 -500 0 1 {name=r11 node="v(xoutd.VM21D)"
descr="v(xoutd.VM21D)"}
C {devices/ngspice_get_value.sym} 3030 -460 0 0 {name=r12 node="i(v.xoutd.v3)"
descr="i(v.xoutd.v3)"}
C {devices/lab_wire.sym} 2970 -650 0 1 {name=l6 sig_type=std_logic lab=VM21S}
C {devices/ngspice_get_value.sym} 2960 -640 0 1 {name=r13 node="v(xoutd.VM21S)"
descr="v(xoutd.VM21S)"}
C {devices/ngspice_get_value.sym} 2990 -240 0 0 {name=r14 node="i(v.xoutd.v4)"
descr="i(v.xoutd.v4)"}
C {devices/vsource.sym} 2970 -280 0 0 {name=V4 value=0}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2630 -550 0 1 {name=M10
L=0.15
W=2
nf=1
mult=160
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2010 -340 0 0 {name=M4
L=0.5
W=2
nf=1
mult=10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2010 -430 0 0 {name=M5
L=0.15
W=2
nf=1
mult=10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2460 -340 0 0 {name=M6
L=0.5
W=2
nf=1
mult=40*10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2950 -430 0 0 {name=M20
L=0.15
W=2
nf=1
mult=10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2950 -340 0 0 {name=M22
L=0.5
W=2
nf=1
mult=10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 2330 -1020 0 0 {name=M13
L=0.15
W=2
nf=1
mult=140*20
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 2590 -1020 0 0 {name=M15
L=0.15
W=2
nf=1
mult=140*20
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 2330 -1110 0 0 {name=M12
L=0.5
W=2
nf=1
mult=140*20
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 2950 -1110 0 0 {name=M19
L=0.5
W=2
nf=1
mult=20
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 2950 -1020 0 0 {name=M23
L=0.15
W=2
nf=1
mult=20
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 2590 -1110 0 0 {name=M14
L=0.5
W=2
nf=1
mult=140*20
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
