v {xschem version=3.1.0 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
T {Bias
240µA} 2410 -650 0 0 0.3 0.3 {}
T {Noise filter} 2560 -500 0 0 0.2 0.2 {}
T {~ 50 Ohm resistors} 5340 -790 0 0 0.4 0.4 {}
N 2350 -230 2430 -230 { lab=VN}
N 2470 -590 2490 -590 { lab=I_Bias}
N 2530 -390 2530 -300 { lab=VM18D}
N 2490 -270 2490 -260 { lab=VN}
N 2490 -360 2490 -330 { lab=#net1}
N 2400 -390 2490 -390 { lab=VN}
N 2400 -390 2400 -260 { lab=VN}
N 2400 -300 2490 -300 { lab=VN}
N 2490 -420 2530 -420 { lab=VM18D}
N 2530 -420 2530 -390 { lab=VM18D}
N 2490 -260 2490 -230 { lab=VN}
N 2400 -260 2400 -230 { lab=VN}
N 2340 -890 2840 -890 { lab=VP}
N 2430 -230 5150 -230 { lab=VN}
N 2490 -440 2490 -420 { lab=VM18D}
N 2400 -470 2490 -470 { lab=VM18D}
N 2530 -500 2530 -470 { lab=I_Bias}
N 2490 -500 2530 -500 { lab=I_Bias}
N 2490 -590 2490 -500 { lab=I_Bias}
N 2580 -470 2580 -460 { lab=I_Bias}
N 2530 -470 2580 -470 { lab=I_Bias}
N 2530 -420 2550 -420 { lab=VM18D}
N 2610 -420 2610 -210 { lab=VM6D}
N 2610 -370 2660 -370 { lab=VM6D}
N 2610 -210 5010 -210 { lab=VM6D}
N 2760 -440 2760 -230 {
lab=VN}
N 5220 -600 5240 -600 {
lab=OutN}
N 5220 -480 5240 -480 {
lab=OutP}
N 2760 -940 2760 -440 {
lab=VN}
N 3120 -250 3120 -230 { lab=VN}
N 2910 -550 2950 -550 { lab=InputSignal}
N 2990 -520 2990 -500 { lab=VM5D}
N 2990 -550 3090 -550 { lab=VN}
N 3250 -520 3250 -500 { lab=VM5D}
N 3250 -620 3250 -580 { lab=V_da2_N}
N 3120 -340 3220 -340 { lab=VN}
N 3120 -430 3220 -430 { lab=VN}
N 3120 -400 3120 -370 { lab=#net2}
N 3080 -430 3080 -340 { lab=VM6D}
N 3220 -430 3220 -230 { lab=VN}
N 2990 -500 3250 -500 { lab=VM5D}
N 3120 -500 3120 -460 { lab=VM5D}
N 3290 -550 3320 -550 { lab=InputRef}
N 3080 -340 3080 -210 { lab=VM6D}
N 3220 -550 3250 -550 {
lab=VN}
N 3090 -550 3220 -550 {
lab=VN}
N 2990 -890 2990 -850 {
lab=VP}
N 3250 -890 3250 -850 {
lab=VP}
N 3250 -850 3250 -840 {
lab=VP}
N 2990 -780 2990 -740 {
lab=V_da2_P}
N 2990 -850 2990 -840 {
lab=VP}
N 3010 -810 3230 -810 {
lab=VN}
N 2760 -940 3130 -940 {
lab=VN}
N 3130 -940 3130 -810 {
lab=VN}
N 4720 -250 4720 -230 { lab=VN}
N 4640 -660 4680 -660 { lab=V_da3_N}
N 4720 -630 4720 -610 { lab=OutP}
N 4720 -660 4820 -660 { lab=VN}
N 4980 -630 4980 -610 { lab=OutN}
N 4980 -730 4980 -690 { lab=VP}
N 4720 -400 4720 -370 { lab=#net3}
N 4680 -430 4680 -340 { lab=VM6D}
N 4680 -340 4680 -210 { lab=VM6D}
N 4950 -660 4980 -660 {
lab=VN}
N 4820 -660 4950 -660 {
lab=VN}
N 4720 -890 4720 -850 {
lab=VP}
N 4720 -740 4720 -690 {
lab=VP}
N 4980 -740 4980 -730 {
lab=VP}
N 4720 -850 4720 -840 {
lab=VP}
N 4980 -600 5070 -600 {
lab=OutN}
N 4720 -480 5070 -480 {
lab=OutP}
N 4720 -600 4720 -460 {
lab=OutP}
N 4720 -610 4720 -600 {
lab=OutP}
N 4980 -250 4980 -230 { lab=VN}
N 4980 -400 4980 -370 { lab=#net4}
N 4980 -600 4980 -460 {
lab=OutN}
N 4980 -610 4980 -600 {
lab=OutN}
N 4720 -840 4720 -740 {
lab=VP}
N 4980 -840 4980 -740 {
lab=VP}
N 4720 -430 4860 -430 {
lab=VN}
N 4860 -430 4860 -230 {
lab=VN}
N 4720 -340 4860 -340 {
lab=VN}
N 4860 -660 4860 -430 {
lab=VN}
N 5010 -210 5080 -210 {
lab=VM6D}
N 5080 -430 5080 -210 {
lab=VM6D}
N 5020 -430 5080 -430 {
lab=VM6D}
N 5020 -340 5080 -340 {
lab=VM6D}
N 4860 -430 4980 -430 {
lab=VN}
N 4860 -340 4980 -340 {
lab=VN}
N 2840 -890 4980 -890 {
lab=VP}
N 2400 -440 2490 -440 {
lab=VM18D}
N 2400 -470 2400 -440 {
lab=VM18D}
N 2550 -420 2580 -420 {
lab=VM18D}
N 2610 -760 2610 -730 {
lab=VN}
N 2610 -730 2760 -730 {
lab=VN}
N 2480 -730 2610 -730 {
lab=VN}
N 2480 -760 2480 -730 {
lab=VN}
N 2480 -890 2480 -820 {
lab=VP}
N 2610 -890 2610 -820 {
lab=VP}
N 4980 -310 4980 -250 {
lab=VN}
N 4720 -310 4720 -250 {
lab=VN}
N 3120 -310 3120 -250 {
lab=VN}
N 5070 -600 5220 -600 {
lab=OutN}
N 5070 -480 5220 -480 {
lab=OutP}
N 4000 -250 4000 -230 { lab=VN}
N 3790 -550 3830 -550 { lab=V_da2_P}
N 3870 -520 3870 -500 { lab=VM15D}
N 3870 -550 3970 -550 { lab=VN}
N 4130 -520 4130 -500 { lab=VM15D}
N 4130 -620 4130 -580 { lab=V_da3_N}
N 4000 -340 4100 -340 { lab=VN}
N 4000 -430 4100 -430 { lab=VN}
N 4000 -400 4000 -370 { lab=#net5}
N 3960 -430 3960 -340 { lab=VM6D}
N 4100 -430 4100 -230 { lab=VN}
N 3870 -500 4130 -500 { lab=VM15D}
N 4000 -500 4000 -460 { lab=VM15D}
N 4170 -550 4200 -550 { lab=V_da2_N}
N 3960 -340 3960 -210 { lab=VM6D}
N 4100 -550 4130 -550 {
lab=VN}
N 3970 -550 4100 -550 {
lab=VN}
N 3870 -890 3870 -850 {
lab=VP}
N 4130 -890 4130 -850 {
lab=VP}
N 4130 -790 4130 -780 {
lab=V_da3_N}
N 4130 -850 4130 -840 {
lab=VP}
N 3870 -780 3870 -730 {
lab=V_da3_P}
N 3870 -850 3870 -840 {
lab=VP}
N 3890 -810 4110 -810 {
lab=VN}
N 4010 -940 4010 -810 {
lab=VN}
N 4000 -310 4000 -250 {
lab=VN}
N 3130 -940 4010 -940 {
lab=VN}
N 3750 -740 3750 -550 {
lab=V_da2_P}
N 3750 -550 3790 -550 {
lab=V_da2_P}
N 4200 -550 4240 -550 {
lab=V_da2_N}
N 4980 -890 4980 -840 {
lab=VP}
N 4360 -730 4360 -660 {
lab=V_da3_N}
N 4130 -730 4360 -730 {
lab=V_da3_N}
N 5020 -660 5050 -660 {
lab=V_da3_P}
N 5050 -750 5050 -660 {
lab=V_da3_P}
N 4100 -550 4100 -430 {
lab=VN}
N 2990 -740 2990 -710 {
lab=V_da2_P}
N 2990 -650 2990 -580 {
lab=V_da2_P}
N 3770 -760 3770 -640 {
lab=V_da2_N}
N 3770 -640 4240 -640 {
lab=V_da2_N}
N 4240 -640 4240 -550 {
lab=V_da2_N}
N 3250 -650 3250 -620 {
lab=V_da2_N}
N 3250 -780 3250 -710 {
lab=V_da2_N}
N 3220 -550 3220 -430 {
lab=VN}
N 4130 -780 4130 -720 {
lab=V_da3_N}
N 3870 -730 3870 -720 {
lab=V_da3_P}
N 3870 -660 3870 -580 {
lab=V_da3_P}
N 4130 -660 4130 -620 {
lab=V_da3_N}
N 5240 -670 5240 -600 {
lab=OutN}
N 5240 -480 5240 -450 {
lab=OutP}
N 5240 -600 5330 -600 {
lab=OutN}
N 5390 -600 5520 -600 {
lab=OutN_R}
N 5360 -580 5360 -240 {
lab=VN}
N 5150 -230 5360 -230 {
lab=VN}
N 5360 -240 5360 -230 {
lab=VN}
N 5460 -480 5530 -480 {
lab=OutP_R}
N 5240 -480 5400 -480 {
lab=OutP}
N 5430 -460 5430 -230 {
lab=VN}
N 5360 -230 5430 -230 {
lab=VN}
N 2990 -740 3470 -740 {
lab=V_da2_P}
N 4360 -660 4510 -660 {
lab=V_da3_N}
N 4570 -660 4640 -660 {
lab=V_da3_N}
N 4360 -750 4500 -750 {
lab=V_da3_P}
N 3870 -750 4360 -750 {
lab=V_da3_P}
N 4560 -750 5050 -750 {
lab=V_da3_P}
N 4510 -660 4570 -660 {
lab=V_da3_N}
N 3470 -740 3510 -740 {
lab=V_da2_P}
N 3510 -740 3590 -740 {
lab=V_da2_P}
N 3670 -760 3770 -760 {
lab=V_da2_N}
N 3650 -740 3750 -740 {
lab=V_da2_P}
N 3250 -760 3600 -760 {
lab=V_da2_N}
N 3590 -740 3650 -740 {
lab=V_da2_P}
N 3610 -760 3670 -760 {
lab=V_da2_N}
N 3600 -760 3610 -760 {
lab=V_da2_N}
N 3320 -550 3380 -550 {
lab=InputRef}
N 3360 -550 3360 -470 {
lab=InputRef}
N 3360 -410 3360 -230 {
lab=VN}
N 2660 -370 2660 -340 {
lab=VM6D}
N 2660 -280 2660 -230 {
lab=VN}
N 3870 -720 3870 -660 {
lab=V_da3_P}
N 4130 -720 4130 -660 {
lab=V_da3_N}
N 2990 -710 2990 -650 {
lab=V_da2_P}
N 3250 -710 3250 -650 {
lab=V_da2_N}
N 4500 -750 4560 -750 {
lab=V_da3_P}
C {devices/iopin.sym} 2350 -230 0 1 {name=p1 lab=VN}
C {devices/ipin.sym} 3380 -550 0 1 {name=p2 lab=InputRef}
C {devices/iopin.sym} 2340 -890 0 1 {name=p4 lab=VP}
C {devices/ipin.sym} 2470 -590 0 0 {name=p5 lab=I_Bias}
C {devices/ipin.sym} 2910 -550 0 0 {name=p6 lab=InputSignal}
C {devices/ngspice_get_value.sym} 2620 -160 0 0 {name=r5 node="@m.x2.xm2.msky130_fd_pr__pfet_01v8_lvt[gm]"
descr="M2 gm"}
C {devices/ngspice_get_value.sym} 2670 -160 0 0 {name=r2 node="@m.x2.xm2.msky130_fd_pr__pfet_01v8_lvt[gds]"
descr="M2 gds"}
C {devices/ngspice_get_value.sym} 2570 -160 0 0 {name=r3 node="@m.x2.xm1.msky130_fd_pr__nfet_01v8_lvt[gm]"
descr="M1 gm"}
C {devices/ngspice_get_value.sym} 2520 -160 0 0 {name=r4 node="@m.x2.xm1.msky130_fd_pr__nfet_01v8_lvt[gds]"
descr="M1 gds"}
C {devices/ngspice_get_value.sym} 2780 -160 0 0 {name=r6 node="@m.x2.xm3.msky130_fd_pr__nfet_01v8[gm]"
descr="M3 gm"}
C {devices/ngspice_get_value.sym} 2730 -160 0 0 {name=r7 node="@m.x2.xm3.msky130_fd_pr__nfet_01v8[gds]"
descr="M3 gds"}
C {devices/opin.sym} 5240 -670 0 0 {name=p8 lab=OutN}
C {devices/opin.sym} 5240 -450 0 0 {name=p9 lab=OutP}
C {sky130_fd_pr/cap_mim_m3_1.sym} 2660 -310 2 0 {name=C3 model=cap_mim_m3_1 W=10 L=16 MF=1 spiceprefix=X}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2510 -300 0 1 {name=M17
L=0.3
W=2
nf=1
mult=6
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2510 -390 0 1 {name=M18
L=0.15
W=2
nf=1
mult=12
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2510 -470 0 1 {name=M7
L=0.15
W=2
nf=1
mult=20
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {devices/lab_wire.sym} 2990 -740 0 0 {name=l2 sig_type=std_logic lab=V_da2_P}
C {devices/lab_wire.sym} 3270 -760 0 1 {name=l5 sig_type=std_logic lab=V_da2_N}
C {devices/ngspice_get_value.sym} 3140 -240 0 0 {name=r14 node="i(v.xoutd.v2)"
descr="i(v.xoutd.v2)"}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3100 -430 0 0 {name=M5
L=0.15
W=2
nf=1
mult=12*5
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3100 -340 0 0 {name=M8
L=0.3
W=2
nf=1
mult=6*5
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {devices/ngspice_get_value.sym} 4720 -530 0 0 {name=r12 node="v(xoutd.v_da3_P)"
descr="v(xoutd.v_da3_P)"}
C {devices/ngspice_get_value.sym} 4980 -530 0 1 {name=r13 node="v(xoutd.v_da3_N)"
descr="v(xoutd.v_da3_N)"}
C {devices/ngspice_get_value.sym} 4740 -240 0 0 {name=r15 node="i(v.xoutd.v3)"
descr="i(v.xoutd.v3)"}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 5000 -660 0 1 {name=M9
L=0.15
W=2
nf=1
mult=120
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 4700 -660 0 0 {name=M11
L=0.15
W=2
nf=1
mult=120
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 4700 -430 0 0 {name=M12
L=0.15
W=2
nf=1
mult=12*10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 4700 -340 0 0 {name=M13
L=0.3
W=2
nf=1
mult=6*10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {devices/ngspice_get_value.sym} 5000 -240 0 0 {name=r17 node="i(v.xoutd.v4)"
descr="i(v.xoutd.v4)"}
C {devices/lab_wire.sym} 3120 -470 0 1 {name=l9 sig_type=std_logic lab=VM5D}
C {devices/ngspice_get_value.sym} 3070 -460 0 1 {name=r18 node="v(xoutd.vm5d)"
descr="v(xoutd.vm5d)"}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 5000 -430 0 1 {name=M1
L=0.15
W=2
nf=1
mult=12*10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 5000 -340 0 1 {name=M3
L=0.3
W=2
nf=1
mult=6*10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2580 -440 1 0 {name=M6
L=8
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {devices/lab_wire.sym} 2490 -420 0 0 {name=l1 sig_type=std_logic lab=VM18D}
C {sky130_fd_pr/cap_mim_m3_1.sym} 2480 -790 2 0 {name=C1 model=cap_mim_m3_1 W=20 L=20 MF=1 spiceprefix=X}
C {sky130_fd_pr/cap_mim_m3_1.sym} 2610 -790 2 0 {name=C2 model=cap_mim_m3_1 W=20 L=20 MF=1 spiceprefix=X}
C {devices/lab_wire.sym} 3970 -750 0 0 {name=l3 sig_type=std_logic lab=V_da3_P}
C {devices/lab_wire.sym} 4320 -730 0 1 {name=l4 sig_type=std_logic lab=V_da3_N}
C {devices/ngspice_get_value.sym} 4020 -240 0 0 {name=r16 node="i(v.xoutd.v2)"
descr="i(v.xoutd.v2)"}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3980 -430 0 0 {name=M15
L=0.15
W=2
nf=1
mult=4*12*5
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3980 -340 0 0 {name=M16
L=0.3
W=2
nf=1
mult=4*6*5
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/res_xhigh_po_1p41.sym} 3870 -810 0 1 {name=R19
W=1.41
L=2
model=res_xhigh_po_1p41
spiceprefix=X
mult=4*2*4}
C {sky130_fd_pr/res_xhigh_po_1p41.sym} 4130 -810 0 0 {name=R20
W=1.41
L=2
model=res_xhigh_po_1p41
spiceprefix=X
mult=4*2*4}
C {devices/lab_wire.sym} 4000 -500 0 1 {name=l6 sig_type=std_logic lab=VM15D}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 4150 -550 0 1 {name=M4
L=0.15
W=2
nf=1
mult=4*10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/res_high_po_1p41.sym} 5360 -600 3 0 {name=R21
W=1.41
L=2
model=res_high_po_1p41
spiceprefix=X
mult=19}
C {devices/opin.sym} 5520 -600 0 0 {name=p3 lab=OutN_R}
C {devices/opin.sym} 5530 -480 0 0 {name=p7 lab=OutP_R}
C {sky130_fd_pr/res_high_po_1p41.sym} 5430 -480 3 0 {name=R22
W=1.41
L=2
model=res_high_po_1p41
spiceprefix=X
mult=19}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2970 -550 0 0 {name=M23
L=0.15
W=2
nf=1
mult=1*12
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3850 -550 0 0 {name=M19
L=0.15
W=2
nf=1
mult=4*10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3270 -550 0 1 {name=M14
L=0.15
W=2
nf=1
mult=1*12
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/res_xhigh_po_1p41.sym} 2990 -810 0 1 {name=R1
W=1.41
L=2
model=res_xhigh_po_1p41
spiceprefix=X
mult=2*4}
C {sky130_fd_pr/res_xhigh_po_1p41.sym} 3250 -810 0 0 {name=R10
W=1.41
L=2
model=res_xhigh_po_1p41
spiceprefix=X
mult=2*4}
C {sky130_fd_pr/cap_mim_m3_1.sym} 3360 -440 2 0 {name=C4 model=cap_mim_m3_1 W=10 L=6 MF=1 spiceprefix=X}
C {devices/lab_wire.sym} 2610 -380 0 1 {name=l7 sig_type=std_logic lab=VM6D}
