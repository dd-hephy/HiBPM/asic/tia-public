v {xschem version=3.1.0 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
T {Bias
240µA} 2410 -650 0 0 0.3 0.3 {}
T {Noise filter} 2560 -500 0 0 0.2 0.2 {}
T {~ 50 Ohm resistors} 6050 -720 0 0 0.4 0.4 {}
N 2350 -230 2430 -230 { lab=VN}
N 2470 -590 2490 -590 { lab=I_Bias}
N 2530 -390 2530 -300 { lab=VM18D}
N 2490 -270 2490 -260 { lab=VN}
N 2490 -360 2490 -330 { lab=#net1}
N 2400 -390 2490 -390 { lab=VN}
N 2400 -390 2400 -260 { lab=VN}
N 2400 -300 2490 -300 { lab=VN}
N 2490 -420 2530 -420 { lab=VM18D}
N 2530 -420 2530 -390 { lab=VM18D}
N 2490 -260 2490 -230 { lab=VN}
N 2400 -260 2400 -230 { lab=VN}
N 2660 -250 2660 -230 { lab=VN}
N 2660 -380 2660 -370 { lab=#net2}
N 2660 -440 2680 -440 { lab=VN}
N 2340 -890 2840 -890 { lab=VP}
N 2430 -230 5770 -230 { lab=VN}
N 2660 -310 2660 -250 { lab=VN}
N 2490 -440 2490 -420 { lab=VM18D}
N 2400 -470 2490 -470 { lab=VM18D}
N 2530 -500 2530 -470 { lab=I_Bias}
N 2490 -500 2530 -500 { lab=I_Bias}
N 2490 -590 2490 -500 { lab=I_Bias}
N 2580 -470 2580 -460 { lab=I_Bias}
N 2530 -470 2580 -470 { lab=I_Bias}
N 2530 -420 2550 -420 { lab=VM18D}
N 2610 -420 2610 -210 { lab=#net2}
N 2610 -370 2660 -370 { lab=#net2}
N 2610 -210 5630 -210 { lab=#net2}
N 2680 -440 2760 -440 {
lab=VN}
N 2760 -440 2760 -230 {
lab=VN}
N 5940 -560 5960 -560 {
lab=OutN}
N 5940 -480 5960 -480 {
lab=OutP}
N 2760 -940 2760 -440 {
lab=VN}
N 3120 -250 3120 -230 { lab=VN}
N 2910 -550 2950 -550 { lab=InputSignal}
N 2990 -520 2990 -500 { lab=VM5D}
N 2990 -550 3090 -550 { lab=VN}
N 3250 -520 3250 -500 { lab=VM5D}
N 3250 -620 3250 -580 { lab=VM19D}
N 3120 -340 3220 -340 { lab=VN}
N 3120 -430 3220 -430 { lab=VN}
N 3120 -400 3120 -370 { lab=#net3}
N 3080 -430 3080 -340 { lab=#net2}
N 3220 -430 3220 -230 { lab=VN}
N 2990 -500 3250 -500 { lab=VM5D}
N 3120 -500 3120 -460 { lab=VM5D}
N 3290 -550 3320 -550 { lab=InputRef}
N 3080 -340 3080 -210 { lab=#net2}
N 3220 -550 3250 -550 {
lab=VN}
N 3090 -550 3220 -550 {
lab=VN}
N 2990 -890 2990 -850 {
lab=VP}
N 3250 -890 3250 -850 {
lab=VP}
N 3250 -850 3250 -840 {
lab=VP}
N 2990 -780 2990 -740 {
lab=V_da2_P}
N 2990 -850 2990 -840 {
lab=VP}
N 3010 -810 3230 -810 {
lab=VN}
N 2760 -940 3130 -940 {
lab=VN}
N 3130 -940 3130 -810 {
lab=VN}
N 5340 -250 5340 -230 { lab=VN}
N 5260 -660 5300 -660 { lab=V_da3_N}
N 5340 -630 5340 -610 { lab=OutP}
N 5340 -660 5440 -660 { lab=VN}
N 5600 -630 5600 -610 { lab=OutN}
N 5600 -730 5600 -690 { lab=VP}
N 5340 -400 5340 -370 { lab=#net4}
N 5300 -430 5300 -340 { lab=#net2}
N 5300 -340 5300 -210 { lab=#net2}
N 5570 -660 5600 -660 {
lab=VN}
N 5440 -660 5570 -660 {
lab=VN}
N 5340 -890 5340 -850 {
lab=VP}
N 5340 -780 5340 -690 {
lab=VP}
N 5600 -780 5600 -730 {
lab=VP}
N 5340 -850 5340 -840 {
lab=VP}
N 5600 -560 5690 -560 {
lab=OutN}
N 5340 -480 5690 -480 {
lab=OutP}
N 5340 -600 5340 -460 {
lab=OutP}
N 5340 -610 5340 -600 {
lab=OutP}
N 5600 -250 5600 -230 { lab=VN}
N 5600 -400 5600 -370 { lab=#net5}
N 5600 -600 5600 -460 {
lab=OutN}
N 5600 -610 5600 -600 {
lab=OutN}
N 5340 -840 5340 -780 {
lab=VP}
N 5600 -840 5600 -780 {
lab=VP}
N 5340 -430 5480 -430 {
lab=VN}
N 5480 -430 5480 -230 {
lab=VN}
N 5340 -340 5480 -340 {
lab=VN}
N 5480 -660 5480 -430 {
lab=VN}
N 5630 -210 5700 -210 {
lab=#net2}
N 5700 -430 5700 -210 {
lab=#net2}
N 5640 -430 5700 -430 {
lab=#net2}
N 5640 -340 5700 -340 {
lab=#net2}
N 5480 -430 5600 -430 {
lab=VN}
N 5480 -340 5600 -340 {
lab=VN}
N 2840 -890 5600 -890 {
lab=VP}
N 2400 -440 2490 -440 {
lab=VM18D}
N 2400 -470 2400 -440 {
lab=VM18D}
N 2550 -420 2580 -420 {
lab=VM18D}
N 2610 -760 2610 -730 {
lab=VN}
N 2610 -730 2760 -730 {
lab=VN}
N 2480 -730 2610 -730 {
lab=VN}
N 2480 -760 2480 -730 {
lab=VN}
N 2480 -890 2480 -820 {
lab=VP}
N 2610 -890 2610 -820 {
lab=VP}
N 5600 -310 5600 -250 {
lab=VN}
N 5340 -310 5340 -250 {
lab=VN}
N 3120 -310 3120 -250 {
lab=VN}
N 5690 -560 5940 -560 {
lab=OutN}
N 5690 -480 5940 -480 {
lab=OutP}
N 4620 -250 4620 -230 { lab=VN}
N 4410 -550 4450 -550 { lab=#net6}
N 4490 -520 4490 -500 { lab=VM15D}
N 4490 -550 4590 -550 { lab=VN}
N 4750 -520 4750 -500 { lab=VM15D}
N 4750 -620 4750 -580 { lab=VM10D}
N 4620 -340 4720 -340 { lab=VN}
N 4620 -430 4720 -430 { lab=VN}
N 4620 -400 4620 -370 { lab=#net7}
N 4580 -430 4580 -340 { lab=#net2}
N 4720 -430 4720 -230 { lab=VN}
N 4490 -500 4750 -500 { lab=VM15D}
N 4620 -500 4620 -460 { lab=VM15D}
N 4790 -550 4820 -550 { lab=#net8}
N 4580 -340 4580 -210 { lab=#net2}
N 4720 -550 4750 -550 {
lab=VN}
N 4590 -550 4720 -550 {
lab=VN}
N 4490 -890 4490 -850 {
lab=VP}
N 4750 -890 4750 -850 {
lab=VP}
N 4750 -790 4750 -780 {
lab=V_da3_N}
N 4750 -850 4750 -840 {
lab=VP}
N 4490 -780 4490 -730 {
lab=V_da3_P}
N 4490 -850 4490 -840 {
lab=VP}
N 4510 -810 4730 -810 {
lab=VN}
N 4630 -940 4630 -810 {
lab=VN}
N 4620 -310 4620 -250 {
lab=VN}
N 3130 -940 4630 -940 {
lab=VN}
N 4370 -740 4370 -550 {
lab=#net6}
N 4370 -550 4410 -550 {
lab=#net6}
N 4820 -550 4860 -550 {
lab=#net8}
N 5600 -890 5600 -840 {
lab=VP}
N 4980 -660 5260 -660 {
lab=V_da3_N}
N 4980 -730 4980 -660 {
lab=V_da3_N}
N 4750 -730 4980 -730 {
lab=V_da3_N}
N 5640 -660 5670 -660 {
lab=V_da3_P}
N 5670 -750 5670 -660 {
lab=V_da3_P}
N 4490 -750 5670 -750 {
lab=V_da3_P}
N 4720 -550 4720 -430 {
lab=VN}
N 2990 -740 2990 -710 {
lab=V_da2_P}
N 2990 -650 2990 -580 {
lab=VM20D}
N 4390 -760 4390 -640 {
lab=#net8}
N 4390 -640 4860 -640 {
lab=#net8}
N 4860 -640 4860 -550 {
lab=#net8}
N 3250 -650 3250 -620 {
lab=VM19D}
N 3250 -780 3250 -710 {
lab=V_da2_N}
N 2850 -680 2950 -680 {
lab=VP}
N 2850 -890 2850 -680 {
lab=VP}
N 3290 -680 3410 -680 {
lab=VP}
N 3410 -890 3410 -680 {
lab=VP}
N 3220 -550 3220 -430 {
lab=VN}
N 4750 -780 4750 -720 {
lab=V_da3_N}
N 4490 -730 4490 -720 {
lab=V_da3_P}
N 4490 -660 4490 -580 {
lab=VM14D}
N 4750 -660 4750 -620 {
lab=VM10D}
N 4430 -890 4430 -700 {
lab=VP}
N 4790 -690 4840 -690 {
lab=VP}
N 4840 -890 4840 -690 {
lab=VP}
N 4720 -690 4720 -550 {
lab=VN}
N 4430 -690 4450 -690 {
lab=VP}
N 4430 -700 4430 -690 {
lab=VP}
N 4490 -690 4750 -690 {
lab=VN}
N 5960 -630 5960 -560 {
lab=OutN}
N 5960 -480 5960 -390 {
lab=OutP}
N 5960 -560 6050 -560 {
lab=OutN}
N 6110 -560 6240 -560 {
lab=OutN_R}
N 6080 -540 6080 -240 {
lab=VN}
N 5770 -230 6080 -230 {
lab=VN}
N 6080 -240 6080 -230 {
lab=VN}
N 6180 -460 6250 -460 {
lab=OutP_R}
N 5960 -460 6120 -460 {
lab=OutP}
N 6150 -440 6150 -230 {
lab=VN}
N 6080 -230 6150 -230 {
lab=VN}
N 3730 -250 3730 -230 { lab=VN}
N 3520 -550 3560 -550 { lab=V_da2_P}
N 3600 -520 3600 -500 { lab=#net9}
N 3600 -550 3700 -550 { lab=VN}
N 3860 -520 3860 -500 { lab=#net9}
N 3860 -620 3860 -580 { lab=#net10}
N 3730 -340 3830 -340 { lab=VN}
N 3730 -430 3830 -430 { lab=VN}
N 3730 -400 3730 -370 { lab=#net11}
N 3690 -430 3690 -340 { lab=#net2}
N 3830 -430 3830 -230 { lab=VN}
N 3600 -500 3860 -500 { lab=#net9}
N 3730 -500 3730 -460 { lab=#net9}
N 3900 -550 3930 -550 { lab=V_da2_N}
N 3690 -340 3690 -210 { lab=#net2}
N 3830 -550 3860 -550 {
lab=VN}
N 3700 -550 3830 -550 {
lab=VN}
N 3600 -890 3600 -850 {
lab=VP}
N 3860 -890 3860 -850 {
lab=VP}
N 3860 -790 3860 -780 {
lab=#net6}
N 3860 -850 3860 -840 {
lab=VP}
N 3600 -780 3600 -730 {
lab=#net8}
N 3600 -850 3600 -840 {
lab=VP}
N 3620 -810 3840 -810 {
lab=VN}
N 3730 -310 3730 -250 {
lab=VN}
N 3480 -550 3520 -550 {
lab=V_da2_P}
N 3930 -550 3970 -550 {
lab=V_da2_N}
N 3830 -550 3830 -430 {
lab=VN}
N 3500 -640 3970 -640 {
lab=V_da2_N}
N 3970 -640 3970 -550 {
lab=V_da2_N}
N 3860 -780 3860 -720 {
lab=#net6}
N 3600 -730 3600 -720 {
lab=#net8}
N 3600 -660 3600 -580 {
lab=#net12}
N 3860 -660 3860 -620 {
lab=#net10}
N 3540 -890 3540 -700 {
lab=VP}
N 3900 -690 3950 -690 {
lab=VP}
N 3950 -890 3950 -690 {
lab=VP}
N 3830 -690 3830 -550 {
lab=VN}
N 3540 -690 3560 -690 {
lab=VP}
N 3540 -700 3540 -690 {
lab=VP}
N 3600 -690 3860 -690 {
lab=VN}
N 3750 -940 3750 -810 {
lab=VN}
N 3130 -680 3130 -550 {
lab=VN}
N 3480 -600 3480 -550 {
lab=V_da2_P}
N 3250 -760 3500 -760 {
lab=V_da2_N}
N 3500 -760 3500 -640 {
lab=V_da2_N}
N 2990 -740 3470 -740 {
lab=V_da2_P}
N 3470 -740 3480 -740 {
lab=V_da2_P}
N 3480 -740 3480 -600 {
lab=V_da2_P}
N 3860 -740 4370 -740 {
lab=#net6}
N 3600 -760 4390 -760 {
lab=#net8}
C {devices/iopin.sym} 2350 -230 0 1 {name=p1 lab=VN}
C {devices/ipin.sym} 3320 -550 0 1 {name=p2 lab=InputRef}
C {devices/iopin.sym} 2340 -890 0 1 {name=p4 lab=VP}
C {devices/ipin.sym} 2470 -590 0 0 {name=p5 lab=I_Bias}
C {devices/ipin.sym} 2910 -550 0 0 {name=p6 lab=InputSignal}
C {devices/ngspice_get_value.sym} 2620 -160 0 0 {name=r5 node="@m.x2.xm2.msky130_fd_pr__pfet_01v8_lvt[gm]"
descr="M2 gm"}
C {devices/ngspice_get_value.sym} 2670 -160 0 0 {name=r2 node="@m.x2.xm2.msky130_fd_pr__pfet_01v8_lvt[gds]"
descr="M2 gds"}
C {devices/ngspice_get_value.sym} 2570 -160 0 0 {name=r3 node="@m.x2.xm1.msky130_fd_pr__nfet_01v8_lvt[gm]"
descr="M1 gm"}
C {devices/ngspice_get_value.sym} 2520 -160 0 0 {name=r4 node="@m.x2.xm1.msky130_fd_pr__nfet_01v8_lvt[gds]"
descr="M1 gds"}
C {devices/ngspice_get_value.sym} 2780 -160 0 0 {name=r6 node="@m.x2.xm3.msky130_fd_pr__nfet_01v8[gm]"
descr="M3 gm"}
C {devices/ngspice_get_value.sym} 2730 -160 0 0 {name=r7 node="@m.x2.xm3.msky130_fd_pr__nfet_01v8[gds]"
descr="M3 gds"}
C {devices/opin.sym} 5960 -630 0 0 {name=p8 lab=OutN}
C {devices/opin.sym} 5960 -390 0 0 {name=p9 lab=OutP}
C {sky130_fd_pr/cap_mim_m3_1.sym} 2660 -340 2 1 {name=C5 model=cap_mim_m3_1 W=20 L=20 MF=1 spiceprefix=X}
C {sky130_fd_pr/cap_mim_m3_1.sym} 2660 -410 0 0 {name=C3 model=cap_mim_m3_1 W=20 L=20 MF=1 spiceprefix=X}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2510 -300 0 1 {name=M17
L=0.3
W=2
nf=1
mult=6
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2510 -390 0 1 {name=M18
L=0.15
W=2
nf=1
mult=12
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2510 -470 0 1 {name=M7
L=0.15
W=2
nf=1
mult=20
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {devices/lab_wire.sym} 2990 -740 0 0 {name=l2 sig_type=std_logic lab=V_da2_P}
C {devices/lab_wire.sym} 3270 -760 0 1 {name=l5 sig_type=std_logic lab=V_da2_N}
C {devices/ngspice_get_value.sym} 3140 -240 0 0 {name=r14 node="i(v.xoutd.v2)"
descr="i(v.xoutd.v2)"}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3100 -430 0 0 {name=M5
L=0.15
W=2
nf=1
mult=12*5
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3100 -340 0 0 {name=M8
L=0.3
W=2
nf=1
mult=6*5
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/res_xhigh_po_1p41.sym} 2990 -810 0 1 {name=R1
W=1.41
L=2
model=res_xhigh_po_1p41
spiceprefix=X
mult=2*4}
C {sky130_fd_pr/res_xhigh_po_1p41.sym} 3250 -810 0 0 {name=R3
W=1.41
L=2
model=res_xhigh_po_1p41
spiceprefix=X
mult=2*4}
C {devices/ngspice_get_value.sym} 5340 -530 0 0 {name=r12 node="v(xoutd.v_da3_P)"
descr="v(xoutd.v_da3_P)"}
C {devices/ngspice_get_value.sym} 5600 -530 0 1 {name=r13 node="v(xoutd.v_da3_N)"
descr="v(xoutd.v_da3_N)"}
C {devices/ngspice_get_value.sym} 5360 -240 0 0 {name=r15 node="i(v.xoutd.v3)"
descr="i(v.xoutd.v3)"}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 5620 -660 0 1 {name=M9
L=0.15
W=2
nf=1
mult=120
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 5320 -660 0 0 {name=M11
L=0.15
W=2
nf=1
mult=120
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 5320 -430 0 0 {name=M12
L=0.15
W=2
nf=1
mult=12*10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 5320 -340 0 0 {name=M13
L=0.3
W=2
nf=1
mult=6*10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {devices/ngspice_get_value.sym} 5620 -240 0 0 {name=r17 node="i(v.xoutd.v4)"
descr="i(v.xoutd.v4)"}
C {devices/lab_wire.sym} 3120 -470 0 1 {name=l9 sig_type=std_logic lab=VM5D}
C {devices/ngspice_get_value.sym} 3070 -460 0 1 {name=r18 node="v(xoutd.vm5d)"
descr="v(xoutd.vm5d)"}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 5620 -430 0 1 {name=M1
L=0.15
W=2
nf=1
mult=12*10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 5620 -340 0 1 {name=M3
L=0.3
W=2
nf=1
mult=6*10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2580 -440 1 0 {name=M6
L=8
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {devices/lab_wire.sym} 2490 -420 0 0 {name=l1 sig_type=std_logic lab=VM18D}
C {sky130_fd_pr/cap_mim_m3_1.sym} 2480 -790 2 0 {name=C1 model=cap_mim_m3_1 W=20 L=20 MF=1 spiceprefix=X}
C {sky130_fd_pr/cap_mim_m3_1.sym} 2610 -790 2 0 {name=C2 model=cap_mim_m3_1 W=20 L=20 MF=1 spiceprefix=X}
C {devices/lab_wire.sym} 4590 -750 0 0 {name=l3 sig_type=std_logic lab=V_da3_P}
C {devices/lab_wire.sym} 5060 -660 0 1 {name=l4 sig_type=std_logic lab=V_da3_N}
C {devices/ngspice_get_value.sym} 4640 -240 0 0 {name=r16 node="i(v.xoutd.v2)"
descr="i(v.xoutd.v2)"}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 4600 -430 0 0 {name=M15
L=0.15
W=2
nf=1
mult=4*12*5
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 4600 -340 0 0 {name=M16
L=0.3
W=2
nf=1
mult=4*6*5
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/res_xhigh_po_1p41.sym} 4490 -810 0 1 {name=R19
W=1.41
L=2
model=res_xhigh_po_1p41
spiceprefix=X
mult=4*2*4}
C {sky130_fd_pr/res_xhigh_po_1p41.sym} 4750 -810 0 0 {name=R20
W=1.41
L=2
model=res_xhigh_po_1p41
spiceprefix=X
mult=4*2*4}
C {devices/lab_wire.sym} 4620 -500 0 1 {name=l6 sig_type=std_logic lab=VM15D}
C {devices/lab_wire.sym} 2990 -610 0 0 {name=l7 sig_type=std_logic lab=VM20D}
C {devices/lab_wire.sym} 3250 -610 0 1 {name=l8 sig_type=std_logic lab=VM19D}
C {devices/lab_wire.sym} 4750 -600 0 1 {name=l10 sig_type=std_logic lab=VM10D}
C {devices/lab_wire.sym} 4490 -600 0 1 {name=l11 sig_type=std_logic lab=VM14D}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 4770 -550 0 1 {name=M4
L=0.15
W=2
nf=1
mult=40
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/res_high_po_1p41.sym} 6080 -560 3 0 {name=R21
W=1.41
L=2
model=res_high_po_1p41
spiceprefix=X
mult=19}
C {devices/opin.sym} 6240 -560 0 0 {name=p3 lab=OutN_R}
C {devices/opin.sym} 6250 -460 0 0 {name=p7 lab=OutP_R}
C {sky130_fd_pr/res_high_po_1p41.sym} 6150 -460 3 0 {name=R22
W=1.41
L=2
model=res_high_po_1p41
spiceprefix=X
mult=19}
C {devices/ngspice_get_value.sym} 3750 -240 0 0 {name=r23 node="i(v.xoutd.v2)"
descr="i(v.xoutd.v2)"}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3710 -430 0 0 {name=M2
L=0.15
W=2
nf=1
mult=2*12*5
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3710 -340 0 0 {name=M10
L=0.3
W=2
nf=1
mult=2*6*5
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/res_xhigh_po_1p41.sym} 3600 -810 0 1 {name=R24
W=1.41
L=2
model=res_xhigh_po_1p41
spiceprefix=X
mult=2*2*4}
C {sky130_fd_pr/res_xhigh_po_1p41.sym} 3860 -810 0 0 {name=R25
W=1.41
L=2
model=res_xhigh_po_1p41
spiceprefix=X
mult=2*2*4}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 2970 -550 0 0 {name=M23
L=0.15
W=2
nf=1
mult=10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 4470 -550 0 0 {name=M19
L=0.15
W=2
nf=1
mult=40
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3270 -550 0 1 {name=M14
L=0.15
W=2
nf=1
mult=10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3580 -550 0 0 {name=M20
L=0.15
W=2
nf=1
mult=2*10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8_lvt.sym} 3880 -550 0 1 {name=M21
L=0.15
W=2
nf=1
mult=2*10
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8_lvt
spiceprefix=X
}
C {devices/res.sym} 2990 -680 0 0 {name=R8
value=0
footprint=1206
device=resistor
m=1}
C {devices/res.sym} 3250 -680 0 0 {name=R9
value=0
footprint=1206
device=resistor
m=1}
C {devices/res.sym} 3600 -690 0 0 {name=R10
value=0
footprint=1206
device=resistor
m=1}
C {devices/res.sym} 3860 -690 0 0 {name=R11
value=0
footprint=1206
device=resistor
m=1}
C {devices/res.sym} 4490 -690 0 0 {name=R26
value=0
footprint=1206
device=resistor
m=1}
C {devices/res.sym} 4750 -690 0 0 {name=R27
value=0
footprint=1206
device=resistor
m=1}
